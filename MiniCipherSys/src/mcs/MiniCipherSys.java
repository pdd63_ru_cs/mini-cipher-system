package mcs;

import java.io.IOException;
import java.util.Scanner;
import java.util.Random;

/**
 * This class implements a mini cipher system.
 * 
 * @author RU NB CS112
 */
public class MiniCipherSys {
	
	/**
	 * Circular linked list that is the sequence of numbers for encryption
	 */
	SeqNode seqRear;
	
	/**
	 * Makes a randomized sequence of numbers for encryption. The sequence is 
	 * stored in a circular linked list, whose last node is pointed to by the field seqRear
	 */
	public void makeSeq() {
		// start with an array of 1..28 for easy randomizing
		int[] seqValues = new int[28];
		// assign values from 1 to 28
		for (int i=0; i < seqValues.length; i++) {
			seqValues[i] = i+1;
		}
		
		// randomize the numbers
		Random randgen = new Random();
 	        for (int i = 0; i < seqValues.length; i++) {
	            int other = randgen.nextInt(28);
	            int temp = seqValues[i];
	            seqValues[i] = seqValues[other];
	            seqValues[other] = temp;
	        }
	     
	    // create a circular linked list from this sequence and make seqRear point to its last node
	    SeqNode sn = new SeqNode();
	    sn.seqValue = seqValues[0];
	    sn.next = sn;
	    seqRear = sn;
	    for (int i=1; i < seqValues.length; i++) {
	    	sn = new SeqNode();
	    	sn.seqValue = seqValues[i];
	    	sn.next = seqRear.next;
	    	seqRear.next = sn;
	    	seqRear = sn;
	    }
	}
	
	/**
	 * Makes a circular linked list out of values read from scanner.
	 */
	public void makeSeq(Scanner scanner) 
	throws IOException {
		SeqNode sn = null;
		if (scanner.hasNextInt()) {
			sn = new SeqNode();
		    sn.seqValue = scanner.nextInt();
		    sn.next = sn;
		    seqRear = sn;
		}
		while (scanner.hasNextInt()) {
			sn = new SeqNode();
	    	sn.seqValue = scanner.nextInt();
	    	sn.next = seqRear.next;
	    	seqRear.next = sn;
	    	seqRear = sn;
		}
	}
	
	/**
	 * Implements Step 1 - Flag A - on the sequence.
	 */
	void flagA() {
	    // COMPLETE THIS METHOD
	    if(seqRear == null)
	    	return;
	    SeqNode prev, curr, succ;
	    prev = seqRear;
	    do{
	    	curr = prev.next;
	    	// check if curr is flagA; swap flagA with next node
	    	if(curr.seqValue == 27){ 
	    		succ = curr.next;
	    		prev.next = succ;
	    		curr.next = succ.next;
	    		succ.next = curr;
	    		if(curr == seqRear)
	    			seqRear = succ;
	    		else if(succ == seqRear)
	    			seqRear = curr;
	    		return;
	    	}
	    	prev = curr;
	    }while(prev != seqRear);
	}
	
	/**
	 * Implements Step 2 - Flag B - on the sequence.
	 */
	void flagB() {
	    // COMPLETE THIS METHOD
	    SeqNode prev, curr, succ, target;
	    prev = seqRear;
	    do{
	    	curr = prev.next;
	    	// check if curr value is flagB and move it down two nodes
	    	if(curr.seqValue == 28){
	    		succ = curr.next;
	    		target = succ.next;
	    		while(prev != target){
	    			prev.next = succ;
	    			curr.next = succ.next;
	    			succ.next = curr;
	    			if(succ == seqRear)
	    				seqRear = curr;
	    			else if(curr == seqRear)
	    				seqRear = succ;
	    			prev = succ;
	    			succ = curr.next;
	    		}
	    		return;
	    	}
	    	prev = curr;
	    }while(prev != seqRear);
	}
	
	/**
	 * Implements Step 3 - Triple Shift - on the sequence.
	 */
	void tripleShift() {
	    // COMPLETE THIS METHOD
	    SeqNode flag1, flag2, prev, succ;
	    prev = seqRear;
	    do{
	    	flag1 = prev.next;
	    	if(flag1.seqValue == 27 || flag1.seqValue == 28){
	    		flag2 = flag1;
	    		while((flag2 = flag2.next).seqValue != 27 && flag2.seqValue != 28);

	    		if(flag1 == seqRear.next){
	    			seqRear = flag2;
	    			return;
	    		}else if(flag2 == seqRear){
	    			seqRear = prev;
	    			return;
	    		}
	    		succ = flag2.next; //[flag1]->...[flag2]->[succ]->...[sR]->
	    		prev.next = succ; //[sR.next]->....[prev]->[succ]->...[sR]->
	    		flag2.next = seqRear.next; //[flag1]->...[flag2]->[sR.next]->...[prev]->[succ]->...[sR]->
	    		seqRear.next = flag1;
	    		seqRear = prev; //[succ]->...[sR]->[flag1]->...[flag2]->[sR.next]->...(sR =[prev])->
	    		return;
	    	}
	    	prev = flag1;
	    }while(prev != seqRear);
	}
	
	/**
	 * Implements Step 4 - Count Shift - on the sequence.
	 */
	void countShift() {		
	    // COMPLETE THIS METHOD
	    int count = seqRear.seqValue;
	    SeqNode first,last, curr, prev;
	    prev = seqRear;
	    first = seqRear.next;
	    last = seqRear;
	    for (int i = 0; (i < count) && (i < 27); i++){
	    	last = last.next;
	    	System.out.println("last: " + last.seqValue);
	    }
	    curr = last.next;
	    System.out.println("curr: " + curr.seqValue);
	    if(curr == seqRear)
	    	return;
	    while(curr != seqRear){
	    	prev.next = curr;
	    	last.next = curr.next;
	    	curr.next = first;
	    	prev = curr;
	    	curr = last.next;
	    }
	}
	
	/**
	 * Gets a key. Calls the four steps - Flag A, Flag B, Triple Shift, Count Shift, then
	 * counts down based on the value of the first number and extracts the next number 
	 * as key. But if that value is 27 or 28, repeats the whole process (Flag A through Count Shift)
	 * on the latest (current) sequence, until a value less than or equal to 26 is found, 
	 * which is then returned.
	 * 
	 * @return Key between 1 and 26
	 */
	int getKey() {
	    // COMPLETE THIS METHOD
	    // THE FOLLOWING LINE HAS BEEN ADDED TO MAKE THE METHOD COMPILE
	    int counter = seqRear.next.seqValue;
	    SeqNode temp = seqRear;
	    for(int i= 0; (i < counter) && (i < 27); i++) {
	    	temp = temp.next;
	    }
		return temp.next.seqValue;
	}
	
	/**
	 * Utility method that prints a circular linked list, given its rear pointer
	 * 
	 * @param rear Rear pointer
	 */
	private static void printList(SeqNode rear) {
		if (rear == null) { 
			return;
		}
		System.out.print(rear.next.seqValue);
		SeqNode ptr = rear.next;
		do {
			ptr = ptr.next;
			System.out.print("," + ptr.seqValue);
		} while (ptr != rear);
		System.out.println("\n");
	}

	/**
	 * Encrypts a message, ignores all characters except upper case letters
	 * 
	 * @param message Message to be encrypted
	 * @return Encrypted message, a sequence of upper case letters only
	 */
	public String encrypt(String message) {	
	    // COMPLETE THIS METHOD
	    // THE FOLLOWING LINE HAS BEEN ADDED TO MAKE THE METHOD COMPILE
		String encryption = "";
		String messagel = message.toLowerCase();
		for(int i = 0; i < messagel.length();i++) {
			int vi = Character.getNumericValue(messagel.charAt(i)) - Character.getNumericValue('a') + 1;
			System.out.println("numvalue = " + vi);
			int key = 27;
			do{
				flagA();
				printList(seqRear);
				flagB();
				printList(seqRear);
				tripleShift();
				printList(seqRear);
				countShift();
				printList(seqRear);
				key = getKey();
			}while(key == 27 || key == 28);
			int sum = vi + key;
			if(sum > 26) {
				sum = sum - 26;
			}
			encryption += Character.toUpperCase(Character.valueOf((char)(('a' + sum)-1)));
		}
		
	    return encryption;
	}
	
	/**
	 * Decrypts a message, which consists of upper case letters only
	 * 
	 * @param message Message to be decrypted
	 * @return Decrypted message, a sequence of upper case letters only
	 */
	public String decrypt(String message) {	
	    // COMPLETE THIS METHOD
	    // THE FOLLOWING LINE HAS BEEN ADDED TO MAKE THE METHOD COMPILE
	    String decryption = "";
	    String messagel = message.toLowerCase();
	    for(int i = 0; i < messagel.length(); i++){
	    	int vi = Character.getNumericValue(messagel.charAt(i)) - Character.getNumericValue('a') + 1;
	    	int key = 27;
	    	do{
				flagA();
				printList(seqRear);
				flagB();
				printList(seqRear);
				tripleShift();
				printList(seqRear);
				countShift();
				printList(seqRear);
				key = getKey();
			}while(key == 27 || key == 28);
			int diff = 0;
			System.out.println("vi = "+ vi + "key = " + key);
			if(vi <= key)
				vi += 26;
			diff = vi - key;
			decryption += Character.toUpperCase(Character.valueOf((char)(('a' + diff) -1))); 
	    }
	    return decryption;
	}
}
